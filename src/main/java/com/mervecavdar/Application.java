package com.mervecavdar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        Long startTime = System.nanoTime();
        SpringApplication.run(Application.class, args);
        Long endTime = System.nanoTime();
        System.out.println("COMPILE TIME -> " + (endTime - startTime));
    }

}
