package com.mervecavdar.service;

import com.mervecavdar.dto.UserDTO;
import com.mervecavdar.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<UserDTO> getAll() {
        return userRepository.findAll().stream().map(UserDTO::toUserDTO).collect(Collectors.toList());
    }

    @Override
    public UserDTO getUserById(Long id) {
        return UserDTO.toUserDTO(userRepository.findById(id).orElseThrow(IllegalArgumentException::new));
    }

    @Override
    public Long save(UserDTO userDTO) {
        return userRepository.save(UserDTO.toUser(userDTO)).getId();
    }

    @Override
    public Long update(UserDTO userDTO) {
        if (!userRepository.findById(userDTO.getId()).isPresent()) {
            throw new IllegalArgumentException();
        }
        return userRepository.save(UserDTO.toUser(userDTO)).getId();
    }

    @Override
    public void delete(Long id) {
        userRepository.delete(userRepository.findById(id).orElseThrow(IllegalArgumentException::new));
    }

}
