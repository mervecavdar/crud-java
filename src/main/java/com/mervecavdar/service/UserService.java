package com.mervecavdar.service;

import com.mervecavdar.dto.UserDTO;

import java.util.List;

public interface UserService {

    List<UserDTO> getAll();

    UserDTO getUserById(Long id);

    Long save(UserDTO userDTO);

    Long update(UserDTO userDTO);

    void delete(Long id);

}
